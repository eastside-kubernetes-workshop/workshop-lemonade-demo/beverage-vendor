package com.ciber.beveragevendor;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import lombok.extern.slf4j.Slf4j;

/***
 * Simplest Beverage Vendor Service
 *
 * Provides a serving of lemonade
 */
@Slf4j
@SpringBootApplication
public class BeverageVendorService {
    public static void main(String[] args) {
        log.info("start - {}", System.getProperty("user.name"));
        System.setProperty("server.port", "9003");
        System.setProperty("spring.output.ansi.enabled", "never");
        System.setProperty("management.endpoints.jmx.exposure.exclude", "*");
        System.setProperty("management.endpoints.web.exposure.include", "info,prometheus,health,env,beans");

        log.info("run");
        SpringApplication application = new SpringApplication(BeverageVendorService.class);
        application.run(args);

    }
}
