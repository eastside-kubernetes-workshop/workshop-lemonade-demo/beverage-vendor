package com.ciber.beveragevendor;

import java.util.Map;

import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import io.micrometer.core.instrument.Counter;
import io.micrometer.core.instrument.MeterRegistry;
import lombok.extern.slf4j.Slf4j;

/***
 * Controller returns a serving of lemonade as in JSON
 *
 */
@Slf4j
@RestController
@RequestMapping("/beverage")
public class BeverageController {

    private final Counter beverageServings;
    private final Counter beverageCost;
    private final Counter beveragePrice;

    public String cupSupply = "http://localhost:9001/cup";
    public String lemonadeSupply = "http://localhost:9002/lemonade";

    public BeverageController(MeterRegistry registry) {
        this.beverageServings = registry.counter(this.getClass().getPackage().getName() + ".beverage.servings");
        this.beverageCost = registry.counter(this.getClass().getPackage().getName() + ".beverage.cost");
        this.beveragePrice = registry.counter(this.getClass().getPackage().getName() + ".beverage.price");

        Map<String, String> env = System.getenv();
        for (String s : env.keySet()) {
            log.info(s + " : " + env.get(s));
        }

        if (!System.getenv("HOME").contains("markhahn")) {
            cupSupply = "http://cup-supply/cup";
            lemonadeSupply = "http://lemonade-supply/lemonade";
        }
        log.info(cupSupply);
        log.info(lemonadeSupply);
    }

    @Bean
    public RestTemplate restTemplate(RestTemplateBuilder builder) {
        return builder.build();
    }

    @GetMapping("/{size}")
    public BeverageInACup getServing(@PathVariable String size, RestTemplate restTemplate) {
        String cupUrl = cupSupply + "/" + size;
        log.info("LemonadeServing {} {}", size, cupUrl);
        ServingCup cup = restTemplate.getForObject(cupUrl, ServingCup.class);
        log.info("ServingCup {}", cup);

        String beverageUrl = lemonadeSupply + "/" + Integer.toString(cup.getOunces());
        log.info("getting beverage {}", beverageUrl);
        BeverageServing serving = restTemplate.getForObject(beverageUrl, BeverageServing.class);
        log.info("BeverageServing {}", serving);

        BeverageInACup cupAndServing = new BeverageInACup(cup, serving);
        log.info("BeverageInACup {}", cupAndServing);

        cupAndServing.setPriceCents(150);
        cupAndServing.setCostCents(cup.getCostCents() + serving.getAmount() * 5);
        cupAndServing.setCostCents(cup.getCostCents() + serving.getAmount() + 50);

        beverageServings.increment();
        beverageCost.increment(cupAndServing.getCostCents());
        beveragePrice.increment(cupAndServing.getPriceCents());

        return cupAndServing;
    }

}
